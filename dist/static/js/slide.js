!
    function($) {
        function change(show, hide) {
            var opts = $(this).data("opts");
            if ("x" == opts.dir) {
                var x = show * opts.width;
                $(this).find(".ck-slide-wrapper").stop().animate({
                    "margin-left": -x
                }, function() {
                    opts.isAnimate = !1
                }), opts.isAnimate = !0
            } else $(this).find(".ck-slide-wrapper li").eq(hide).stop().animate({
                left:'-999px'
            }), $(this).find(".ck-slide-wrapper li").eq(show).show().css({
                left:'-999px'
            }).stop().animate({
                left:'0'
            });
            $(this).find(".ck-slidebox li").removeClass("current"), $(this).find(".ck-slidebox li").eq(show).addClass("current")
        }
        $.fn.ckSlide = function(opts) {
            opts = $.extend({}, $.fn.ckSlide.opts, opts), this.each(function() {
                var slidewrap = $(this).find(".ck-slide-wrapper"),
                    slide = slidewrap.find("li"),
                    count = slide.length,
                    that = this,
                    index = 0,
                    time = null;

                function startAtuoPlay() {
                    opts.autoPlay && (time = setInterval(function() {
                        var old = index;
                        count - 1 <= index ? index = 0 : index++, change.call(that, index, old)
                    }, 4e3))
                }
                $(this).data("opts", opts), $(this).find(".ck-next").on("click", function() {
                    if (1 != opts.isAnimate) {
                        var old = index;
                        count - 1 <= index ? index = 0 : index++, change.call(that, index, old)
                    }
                }), $(this).find(".ck-prev").on("click", function() {
                    if (1 != opts.isAnimate) {
                        var old = index;
                        index <= 0 ? index = count - 1 : index--, change.call(that, index, old)
                    }
                }), $(this).find(".ck-slidebox li").each(function(cindex) {
                    $(this).on("click.slidebox", function() {
                        change.call(that, cindex, index), index = cindex
                    })
                }), $(this).on("mouseover", function() {
                    opts.autoPlay && clearInterval(time), $(this).find(".ctrl-slide").css({
                        opacity: .6
                    })
                }), $(this).on("mouseleave", function() {
                    opts.autoPlay && startAtuoPlay(), $(this).find(".ctrl-slide").css({
                        opacity: .15
                    })
                }), startAtuoPlay();
                var box = $(this).find(".ck-slidebox");
                switch (box.css({
                    "margin-left": -box.width() / 2
                }), opts.dir) {
                    case "x":
                        opts.width = $(this).width(), slidewrap.css({
                            width: count * opts.width
                        }), slide.css({
                            float: "left",
                            position: "relative"
                        }), slidewrap.wrap('<div class="ck-slide-dir"></div>'), slide.show()
                }
            })
        }, $.fn.ckSlide.opts = {
            autoPlay: !1,
            dir: null,
            isAnimate: !1
        }
    }(jQuery);