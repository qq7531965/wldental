/**
 * Created by Administrator on 2018/3/30 0030.
 */

const crypto = require('crypto');
const Sequelize = require('sequelize');
const sequelize = require('../model/database.js');

const account = sequelize.define('account', {
    username: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    }
}, {
    freezeTableName: true
});

async function create() {
    await account.sync();
    let users = await account.findAll();
    if (users.length) {
        return;
    }
    let username = 'wlkq';
    let password = 'wlkq38370866';
    let pw_md5 = crypto.createHash('md5').update(password,'utf-8').digest('hex');
    await  account.create({
        username,
        password:pw_md5
    });
    console.log(`创建成功!用户名:${username},密码:${password}`);
}
create().catch(e =>{
    console.error(`出现错误:${e.message}`);
}).then(() =>{
   sequelize.close();
});