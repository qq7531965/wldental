/**
 * Created by Administrator on 2018/2/27 0027.
 */
module.exports = function () {
    const express = require('express');
    const bodyParser = require('body-parser');
    const session = require('express-session');
    const redisClient = require('./model/redis');
    const redisStore = require('connect-redis')(session);
    const ueditor = require('ueditor');
    const path = require('path');
    const utils = require('./utils/index');
    const favicon = require('serve-favicon');
    // const compression = require('compression');

    const app = express();
    // app.use(compression()); //开启gzip压缩
    //X-Powered-By是网站响应头信息其中的一个，出于安全的考虑，一般会修改或删除掉这个信息
    app.disable('x-powered-by');
    //解析html模板引擎
    const template = require('art-template');
    template.defaults.extname = '.html';
    //缓存
    template.defaults.cache = true;
    //分页
    template.defaults.imports.pagination = utils.pagination;
    //去掉art语法，主要是防止和vue冲突
    template.defaults.rules = [template.defaults.rules[0]];

    //设置视图引擎
    app.set('view engine', 'html');
    app.engine('html', require('express-art-template'));

    app.use(favicon(path.join(__dirname, 'favicon.ico')));
    //托管静态文件
    app.use('/static', express.static('static'));
    app.use('/public', express.static('public'));

    // parse application/x-www-form-urlencoded
    //返回的对象是一个键值对，当extended为false的时候，键值对中的值就为'String'或'Array'形式，为true的时候，则可为任何数据类型。
    app.use(bodyParser.urlencoded({extended: true}));
    //parse application/json
    app.use(bodyParser.json());

    //session内存存储,必须放在路由配置前
    app.use(session({
        name: 'sid',
        secret: 'wldental',
        store: new redisStore({
            client: redisClient
        }),
        cookie: {
            maxAge: 24 * 60 * 60 * 1000 //一天
        },
        resave: false,//设为false redis模板有touch方法
        saveUninitialized: false //设为false,不保存尚未登录的session,省点内存

    }));
    //web 首页
    app.use(require('./router/web/index'));
    //404
    app.use(require('./router/web/404'));
    //美白修复
    app.use(require('./router/web/beauty'));
    //牙齿矫正
    app.use(require('./router/web/braces'));
    //案例列表
    app.use(require('./router/web/case'));
    //案例详情
    app.use(require('./router/web/case-detail'));
    //联系我们
    app.use(require('./router/web/contact_us'));
    //医生列表
    app.use(require('./router/web/doctor'));
    //医生详情
    app.use(require('./router/web/doctor_detail'));
    //种植牙
    app.use(require('./router/web/implant'));
    //资讯列表
    app.use(require('./router/web/news'));
    //资讯详情
    app.use(require('./router/web/news_detail'));
    //牙齿修复
    app.use(require('./router/web/pediatric'));
    //门诊简介
    app.use(require('./router/web/synopsis'));
    //洁牙保健
    app.use(require('./router/web/toothwash'));
    //微创拔牙
    app.use(require('./router/web/exelcymosis'));

    //admin 后台首页
    app.use(require('./router/admin/index'));
    //新增资讯
    app.use(require('./router/admin/article'));
    //资讯列表
    app.use(require('./router/admin/article_list'));
    //新增案例
    app.use(require('./router/admin/case'));
    //案例列表
    app.use(require('./router/admin/case_list'));
    //新增医师
    app.use(require('./router/admin/doctor'));
    //医师列表
    app.use(require('./router/admin/doctor_list'));
    //登录
    app.use(require('./router/admin/login'));
    //修改密码
    app.use(require('./router/admin/password'));

    //api index
    app.use(require('./router/api/index'));
    //api article
    app.use(require('./router/api/article'));
    //api case
    app.use(require('./router/api/case'));
    //api doctor
    app.use(require('./router/api/doctor'));

    const ueditorConfig = require('./config/ueditor.config');

    app.use('/ueditor/ue', ueditor(path.join(__dirname, 'public'), (req, res) => {
        if (!req.session || !req.session.isLogin) {
            return res.json({msg: 'error'});
        }
        //ueditor 客户发起上传图片请
        if (req.query.action === 'uploadimage') {
            // 下面填写你要把图片保存到的路径 （ 以 path.join(__dirname, 'public') 作为根路径）
            res.ue_up('/');//你只要输入要保存的地址 。保存操作交给ueditor来做
        }
        //客户端发起图片列表请求
        else if (req.query.action === 'listimage') {
            res.ue_list('/');// 客户端会列出 dir_url 目录下的所有图片
        }
        //客户端发起其他的请求
        else {
            res.json(ueditorConfig);
        }
    }));
    //启动node app.js 可以使用 supervisor app.js监听 ctrl+s可以刷新
    const port = '8000';
    app.listen(port, () => {
        console.log(`server is listening on port:${port}`);
    });

    // const port = 8090;
    // app.listen(port, '172.18.43.193', () => {
    //     console.log(`server is listening on port: ${port}`);
    // });

    process.on('uncaughtException', (e) => {
        console.log(e.stack);
    });

};