/**
 * Created by Administrator on 2018/3/29 0029.
 */

const router = require('express').Router();

router.get('/admin/login', async(req, res) => {
    res.render('admin/login');
});

module.exports = router;
