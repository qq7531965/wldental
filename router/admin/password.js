/**
 * Created by Administrator on 2018/3/29 0029.
 */
const router = require('express').Router();

router.get('/admin/password', async(req, res) => {
    res.render('admin/password');
});

module.exports = router;