/**
 * Created by Administrator on 2018/3/29 0029.
 */

const router = require('express').Router();
const doctor = require('../../model/doctor');

router.get('/admin/doctor', async(req, res) => {
    if (!req.session || !req.session.isLogin) {
        return res.redirect('/admin/login');
    }
    let data = {
        id: 0,
        name: '',
        type: '',
        content: '',
        skilled: '',
        duty: '',
        brief: '',
        sort: 0
    };

    let id = parseInt(req.query.id);
    if (id) {
        let doctorData = null;
        try {
            doctorData = await doctor.findById(id);
        } catch (e) {
            console.error(`/admin/doctor 查询失败哦 ${e.message}`);
        }
        if (doctorData){
            data = doctorData.toJSON();
        }
    }

    res.render('admin/doctor', data);
});

module.exports = router;