/**
 * Created by Administrator on 2018/3/29 0029.
 */

const router = require('express').Router();
const case_model = require('../../model/case_model');

router.get('/admin/case', async(req, res) => {
    if (!req.session || !req.session.isLogin) {
        return res.redirect('/admin/login');
    }
    let data = {
        id: 0,
        type: '',
        title: '',
        keyword: '',
        content: '',
        sort: 0
    };

    let id = parseInt(req.query.id);
    // console.log(id);
    if (id) {
        let caseData = null;
        try {
            caseData = await case_model.findById(id);
        } catch (e) {
            console.error(`/admin/case 查询失败!`);
        }

        if (caseData) {
            data = caseData.toJSON();
        }
    }
    res.render('admin/case', data);
});

module.exports = router;