/**
 * Created by Administrator on 2018/3/29 0029.
 */
const router = require('express').Router();

router.get(/\/admin(\/index)?\/?$/, async(req, res) => {
    if (!req.session || !req.session.isLogin) {
        return res.redirect('/admin/login');
    }

    res.render('admin/index');
});

module.exports = router;