/**
 * Created by Administrator on 2018/3/29 0029.
 */

const router = require('express').Router();
const article = require('../../model/article');
const utils = require('../../utils/index');

router.get('/admin/article_list', async(req, res) => {
    if (!req.session || !req.session.isLogin) {
        return res.redirect('/admin/login');
    }

    let query = req.query;
    let page = parseInt(query.page) || 1;
    let size = parseInt(query.size) || 10;

    let result = null;
    try {
        result = await article.findAndCountAll({
            limit: size,
            offset: size * (page - 1),
            order: [
                ['id', 'DESC']
            ]
        });
    } catch (e) {
        console.error(`/admin/article_list 查询失败: ${e.message}`);
        result = {count: 0, rows: []};
    }

    let data = {};
    data.count = result.count;
    //map 映射成新的对象row
    data.rows = result.rows.map(row => {
        row = row.toJSON();
        let d = row.updatedAt;
        row.updatedAt = `${d.getFullYear()}-${utils.formatNum(d.getMonth() + 1)}-${d.getDate()}`;
        return row;
    });

    data.page = page;
    data.size = size;
    res.render('admin/article_list', data);
});

module.exports = router;