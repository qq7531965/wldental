/**
 * Created by Administrator on 2018/3/29 0029.
 */
const router = require('express').Router();
const doctor = require('../../model/doctor');
const utils = require('../../utils/index');

router.get('/admin/doctor_list', async(req, res) => {
    if (!req.session || !req.session.isLogin) {
        return res.redirect('/admin/login');
    }
    let query = req.query;
    let page = parseInt(query.page) || 1;
    let size = parseInt(query.size) || 6;

    let result = null;
    try {
        result = await doctor.findAndCountAll({
            limit: size,
            offset: size * (page - 1),
            order: [
                ['sort', 'DESC'],
                ['updatedAt', 'DESC']
            ]
        });
    } catch (e) {
        console.log(`/admin/doctor-list 查询失败:${e.message}`);
        result = {count: 0, rows: []};
    }

    let data = {};

    data.count = result.count;
    let htmlReg = /<((?:"[^"]*"|'[^']*'|[^'">])*)>/g;
    let srcReg = /\s+src="(\/public\/[^"]+)"/;
    //用map把当前对象映射成新的对象
    data.rows = result.rows.map(row => {
        row = row.toJSON();
        let d = row.updatedAt;
        row.updatedAt = `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()}`;
        row.content = row.content.replace(htmlReg, (_, val)=> {
            if (!row.thumbnail && val.slice(0, 3) === 'img' && srcReg.test(val)) {
                row.thumbnail = RegExp.$1;
            }
            return '';
        });
        return row;
    });
    data.page = page;
    data.size = size;
    res.render('admin/doctor_list', data);
});

module.exports = router;