/**
 * Created by Administrator on 2018/3/29 0029.
 */

const router = require('express').Router();
const article = require('../../model/article');

router.get('/admin/article', async(req, res) => {
    if (!req.session || !req.session.isLogin) {
        return res.redirect('/admin/login');
    }
    let data = {
        id: 0,
        type: '',
        title: '',
        keyword: '',
        content: '',
        clickNum: 0,
        sort: 0,
    };
    let id = parseInt(req.query.id);
    if (id) {
        let articleData = null;
        try {
            articleData = await article.findById(id);
        } catch (e) {
            console.error(`/admin/article 查询失败!`)
        }
        if (articleData) {
            data = articleData.toJSON();
        }
    }
    res.render('admin/article', data);
});

module.exports = router;

