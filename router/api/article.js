/**
 * Created by Administrator on 2018/4/2 0002.
 */
const router = require('express').Router();
const article = require('../../model/article');


//创建新增资讯
router.post('/api/article/create', async(req, res) => {
    if (!req.session || !req.session.isLogin) {
        return res.json({code: 1, msg: '尚未登录!'});
    }

    let data = req.body;
    let type = data.type || '资讯';
    let title = data.title;
    let keyword = data.keyword;
    let content = data.content;
    let clickNum = parseInt(data.clickNum) || 0;
    let sort = parseInt(data.sort) || 0;

    if (!title || !content || !keyword) {
        return res.json({code: 0, msg: '信息不完整!'});
    }

    let result = null;
    try {
        result = await article.create({
            type,
            title,
            keyword,
            content,
            clickNum,
            sort
        });
    } catch (e) {
        console.error(`/api/article/create 添加失败 :${e.message}`);
        return res.json({code: 0, msg: '添加失败'});
    }

    result = result.toJSON();

    return res.json({code: 200, msg: '添加成功!', articleId: result.id});
});

//修改 更新资讯
router.post('/api/article/update', async(req, res) => {
    if (!req.session || !req.session.isLogin) {
        return res.json({code: 1, msg: '尚未登录!'});
    }

    let data = req.body;
    let id = parseInt(data.id);

    if (!id) {
        return res.json({code: 0, msg: '缺少必要参数:id'});
    }

    let type = data.type || '资讯';
    let title = data.title;
    let keyword = data.keyword;
    let content = data.content;
    let clickNum = parseInt(data.clickNum) || 0;
    let sort = parseInt(data.sort);

    if (!content || !title || !keyword || !content) {
        return res.json({code: 0, msg: '内容不完整!'});
    }

    let result = [0];
    try {
        result = await article.update({
            type,
            title,
            keyword,
            content,
            clickNum,
            sort
        }, {
            where: {id}
        });
    } catch (e) {
        console.error(`/api/article/update 修改失败:${e.message}`);
        return res.json({code: 0, msg: '修改失败！'});
    }

    if (!result[0]) {
        return res.json({code: 0, msg: '修改失败'});
    }

    return res.json({code: 200, msg: '修改成功!', articleId: id});
});

//删除资讯
router.post('/api/article/delete', async(req, res)=> {
    if (!req.session || !req.session.isLogin) {
        return res.json({code: 1, msg: '尚未登录!'});
    }

    let data = req.body;
    let id = parseInt(data.id);

    if (!id) {
        return res.json({code: 0, msg: '缺少必要参数:id'});
    }

    try {
        await article.destroy({
            where: {id}
        });
    } catch (e) {
        console.error(`/api/article/delete 删除失败！: ${e.message}`);
        return res.json({code: 0, msg: '删除失败！'});
    }
    return res.json({code: 200, msg: '删除成功!！'});
});

//文章点击
router.post('/api/article/clickNum', async(req, res) => {

    let data = req.body;
    let id = parseInt(data.id);

    if (!id) {
        return res.json({code: 0, msg: '缺少必要参数:id'});
    }
    let clickNum = parseInt(data.clickNum);
    clickNum++;
    let result = [0];
    try {
        result = await article.update({
            clickNum
        }, {
            where: {id}
        });
    } catch (e) {
        console.error(`/api/article/clickNum 修改失败:${e.message}`);
        return res.json({code: 0, msg: '修改失败！'});
    }

    if (!result[0]) {
        return res.json({code: 0, msg: '修改失败'});
    }

    return res.json({code: 200, msg: '修改成功!'});
});

module.exports = router;