/**
 * Created by Administrator on 2018/4/7 0007.
 */

const router = require('express').Router();
const case_model = require('../../model/case_model');

//新建案例
router.post('/api/case/create', async(req, res) => {
    if (!req.session || !req.session.isLogin) {
        return res.json({code: 1, msg: '尚未登录'});
    }

    let data = req.body;
    let type = data.type || '案例';
    let title = data.title;
    let keyword = data.keyword;
    let content = data.content;
    let sort = parseInt(data.sort) || 0;

    if (!type || !title || !keyword || !content) {
        return res.json({code: 0, msg: '相关内容不能为空！'});
    }

    let result = null;
    try {
        result = await case_model.create({
            type,
            title,
            keyword,
            content,
            sort
        });
    } catch (e) {
        console.error(`/api/case/create 添加失败 :${e.message}`);
        return res.json({code: 0, msg: '添加失败'});
    }

    result = result.toJSON();
    return res.json({code: 200, msg: '添加成功!', caseId: result.id});
});

//更新 修改案例

router.post('/api/case/update', async(req, res) => {
    if (!req.session || !req.session.isLogin) {
        return res.json({code: 1, msg: '尚未登录'});
    }

    let data = req.body;
    let id = parseInt(data.id);

    if (!id) {
        return res.json({code: 0, msg: '缺少必要参数:id'});
    }

    let type = data.type || '案例';
    let title = data.title;
    let keyword = data.keyword;
    let content = data.content;
    let sort = parseInt(data.sort);

    if (!content || !title || !keyword || !content) {
        return res.json({code: 0, msg: '内容不完整!'});
    }

    let result = [0];

    try {
        result = await case_model.update({
            type,
            title,
            keyword,
            content,
            sort
        }, {
            where: {id}
        });
    } catch (e) {
        console.error(`/api/case/update 修改失败:${e.message}`);
        return res.json({code: 0, msg: '修改失败！'});
    }

    if (!result[0]) {
        return res.json({code: 0, msg: '修改失败！'});
    }

    return res.json({code: 200, msg: '修改成功!！', caseId: id});
});

//删除案例
router.post('/api/case/delete', async(req, res) => {
    if (!req.session || !req.session.isLogin) {
        return res.json({code: 1, msg: '尚未登录!'});
    }

    let data = req.body;
    let id = parseInt(data.id);

    if (!id) {
        return res.json({code: 0, msg: '缺少必要参数:id'});
    }

    try {
        await case_model.destroy({
            where: {id}
        });
    } catch (e) {
        console.error(`/api/case/delete 删除失败！: ${e.message}`);
        return res.json({code: 0, msg: '删除失败！'});
    }
    return res.json({code: 200, msg: '删除成功!！'});
});

//点击量
router.post('/api/case/sort', async(req, res) => {

    let data = req.body;
    let id = parseInt(data.id);

    if (!id) {
        return res.json({code: 0, msg: '缺少必要参数:id'});
    }
    let sort = parseInt(data.sort);
    sort++;
    let result = [0];
    try {
        result = await case_model.update({
            sort
        }, {
            where: {id}
        });
    } catch (e) {
        console.error(`/api/article/sort 修改失败:${e.message}`);
        return res.json({code: 0, msg: '修改失败！'});
    }

    if (!result[0]) {
        return res.json({code: 0, msg: '修改失败'});
    }

    return res.json({code: 200, msg: '修改成功!'});
});

module.exports = router;