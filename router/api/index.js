/**
 * Created by Administrator on 2018/3/26 0026.
 */
const router = require('express').Router();
const crypto = require('crypto');
const account = require('../../model/account');

//判断ue是否存在内容
function handleXSS(content) {
    if (content.indexOf('</script>') === -1) {
        return content;
    }
    let scriptReg = /<script[^>]*>[\s\S]*?<\/script>/g;
    return content.replace(scriptReg, '');
}

//登录接口
router.post('/api/login', async(req, res) => {
    let username = req.body.username;
    let password = req.body.password;

    if (!username || !password) {
        return res.json({code: 0, msg: '请输入用户名或密码!'});
    }

    let user = await account.findOne({where: {username}});

    if (!user) {
        return res.json({code: 0, msg: '该用户不存在!'});
    }

    let md5 = crypto.createHash('md5');

    password = md5.update(password, 'utf-8').digest('hex');

    if (password !== user.password) {
        return res.json({code: 0, msg: '密码错误!'});
    }

    user = user.toJSON();

    //设置session
    req.session.isLogin = 1;
    req.session.userId = user.id;

    return res.json({code: 200, msg: '登录成功!', href: '/admin'});
});

//注销接口 退出登录
router.post('/api/logout', async (req, res) => {
    if (!req.session || !req.session.isLogin) {
        return res.json({code: 200, msg: '注销成功'});
    }

    req.session.destroy(err => {
        if (err) {
            return res.json({code: 0, msg: '注销失败'});
        }

        return res.json({code: 200, msg: '注销成功'});
    });
});

//修改密码
router.post('/api/password', async(req, res) => {
    if (!req.session || !req.session.isLogin) {
        return res.json({code: 1, msg: '尚未登录!'});
    }

    let data = req.body;
    let password = data.password;
    let newPwd = data.newPassword;
    let confirmPwd = data.confirmPassword;

    if (!password || !newPwd || !confirmPwd) {
        return res.json({code: 0, msg: '相关密码不能为空!'});
    }

    if (newPwd !== confirmPwd) {
        return res.json({code: 0, msg: '新密码和确认密码不一致!'});
    }

    let user = null;
    try {
        user = await account.findById(req.session.userId);
    } catch (e) {
        console.error(`/api/password查询出错:${e.message}`);
        return res.json({code: 0, msg: '修改失败!'});
    }

    if (!user) {
        return res.json({code: 0, msg: '用户不存在!'});
    }

    user = user.toJSON();
    password = crypto.createHash('md5').update(password, 'utf-8').digest('hex');

    if (password !== user.password) {
        return res.json({code: 0, msg: '密码不对!'});
    }
    newPwd = crypto.createHash('md5').update(newPwd, 'utf-8').digest('hex');

    try {
        await account.update({
            password: newPwd
        }, {
            where: {id: user.id}
        });
    } catch (e) {
        console.error(`/api/password 密码修改失败:${e.message}`);
        return res.json({code: 0, msg: '修改失败!'});
    }

    return res.json({code: 200, msg: '修改成功!'});
});

module.exports = router;

