/**
 * Created by Administrator on 2018/4/8 0008.
 */
const router = require('express').Router();
const doctor = require('../../model/doctor');

//新增医师
router.post('/api/doctor/create', async(req, res) => {
    if (!req.session || !req.session.isLogin) {
        return res.json({code: 1, msg: '尚未登录!'});
    }

    let data = req.body;
    let name = data.name;
    let type = data.type;
    let content = data.content;
    let skilled = data.skilled;
    let duty = data.duty;
    let brief = data.brief;
    let sort = parseInt(data.sort) || 0;
    if (!name || !type || !content || !skilled || !duty || !brief) {
        return res.json({code: 0, msg: '信息填写不完整！'});
    }

    let result = null;
    try {
        result = await doctor.create({
            name,
            type,
            content,
            skilled,
            duty,
            brief,
            sort
        });
    } catch (e) {
        console.error(`/api/doctor/create 添加失败: ${e.message}`);
        return res.json({code: 0, msg: '添加失败'});
    }

    result = result.toJSON();
    return res.json({code: 200, msg: '添加成功!', doctorId: result.id});
});

//更新 修改医师
router.post('/api/doctor/update', async(req, res)=> {
    if (!req.session || !req.session.isLogin) {
        return res.json({code: 1, msg: '尚未登录!'});
    }

    let data = req.body;
    let id = parseInt(data.id);
    if (!id) {
        return res.json({code: 0, msg: '缺少必要参数id'});
    }
    let name = data.name;
    let type = data.type;
    let content = data.content;
    let skilled = data.skilled;
    let duty = data.duty;
    let brief = data.brief;
    let sort = parseInt(data.sort);
    if (!name || !type || !content || !skilled || !duty || !brief) {
        return res.json({code: 0, msg: '信息填写不完整！'});
    }

    let result = [0];
    try {
        result = await doctor.update({
            name,
            type,
            content,
            skilled,
            duty,
            brief,
            sort
        }, {
            where: {id}
        });
    } catch (e) {
        console.error(`/api/doctor/update 修改失败: ${e.message}`);
        return res.json({code: 0, msg: '修改失败'});
    }
    //id没有记录
    if (!result[0]) {
        return res.json({code: 0, msg: '修改失败!'});
    }
    return res.json({code: 200, msg: '修改成功！', doctorId: id});
});

//删除医师记录
router.post('/api/doctor/delete', async(req, res) => {
    if (!req.session || !req.session.isLogin) {
        return res.json({code: 1, msg: '尚未登录!'});
    }

    let data = req.body;
    let id = parseInt(data.id);
    console.log(id);
    if (!id) {
        return res.json({code: 0, msg: '缺少必要参数:id'});
    }

    try {
        await doctor.destroy({
            where: {id}
        });
    } catch (e) {
        console.error(`/api/doctor/delete 删除失败!: ${e.message}`);
        return res.json({code: 0, msg: '删除失败!'});
    }

    return res.json({code: 200, msg: '删除成功!'});
});

//点击量
router.post('/api/doctor/sort', async(req, res) => {

    let data = req.body;
    let id = parseInt(data.id);

    if (!id) {
        return res.json({code: 0, msg: '缺少必要参数:id'});
    }
    let sort = parseInt(data.sort);
    sort++;
    let result = [0];
    try {
        result = await doctor.update({
            sort
        }, {
            where: {id}
        });
    } catch (e) {
        console.error(`/api/article/sort 修改失败:${e.message}`);
        return res.json({code: 0, msg: '修改失败！'});
    }

    if (!result[0]) {
        return res.json({code: 0, msg: '修改失败'});
    }

    return res.json({code: 200, msg: '修改成功!'});
});

module.exports = router;