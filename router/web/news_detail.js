/**
 * Created by Administrator on 2018/3/29 0029.
 */
const router = require('express').Router();
const article = require('../../model/article');
const utils = require('../../utils/index');


router.get('/news_detail', async(req, res) => {
    let id = parseInt(req.query.id);

    if (!id) {
        return res.redirect('/404');
    }

    let result = null, list = null,list_right = null, prev = null, next = null;
    try {
        [result, list,list_right, prev, next] = await Promise.all([
            article.findById(id),

            article.findAll({
                where:{
                    type:{
                        $ne:'热门问答'
                    }
                },
                limit: 5,
                offset: 0,
                order: [
                    ['sort', 'DESC'],
                    ['updatedAt', 'DESC']
                ]
            }),
            article.findAll({
                where:{
                    type:'热门问答'
                },
                limit: 5,
                offset: 0,
                order: [
                    ['sort', 'DESC'],
                    ['updatedAt', 'DESC']
                ]
            }),
            // 上一篇
            article.max('id', {
                where: {
                    id: {$lt: id}
                }
            }),
            // 下一篇
            article.min('id', {
                where: {
                    id: {$gt: id}
                }
            })
        ]);
    } catch (e) {
        console.error(`/web/news_detail 查询失败：${e.message}`);

        return res.redirect('/404');
    }

    if (!result) {
        return res.redirect('/404');
    }

    // let prevResult = null, nextResult = null;
    let data = {};
    let htmlReg = /<((?:"[^"]*"|'[^']*'|[^'">])*)>/g;

    data.prev = prev || id;
    data.next = next || id;

    data.list = list.map(p => {
        return p.toJSON();
    });

    data.list_right = list_right.map(r => {
        return r.toJSON();
    });

    data.article = result.toJSON();

    let meta = data.article.content.replace(htmlReg,'undefined');
    data.article.content_meta= meta.slice(0,100);

    let d = data.article.updatedAt;
    data.article.updatedAt = `${d.getFullYear()}-${utils.formatNum(d.getMonth() + 1)}-${d.getDate()}`;
    console.log(data.article.updatedAt);
    res.render('web/news_detail', data);

});

module.exports = router;
