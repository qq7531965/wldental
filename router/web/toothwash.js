/**
 * Created by Administrator on 2018/3/29 0029.
 */
const router = require('express').Router();
const article = require('../../model/article');

router.get('/toothwash', async(req, res) => {

    let result = null;
    try {
        result = await article.findAll({
            where: {
                type: '洗牙'
            },
            limit: 24,
            offset: 0,
            order: [
                ['sort', 'DESC'],
                ['updatedAt', 'DESC']
            ]
        });
    } catch (e) {
        result = {count:0,rows:[]};
        console.error(`/web/toothwash 查询出错！${e.message}`);
    }
    let data = {};
    data.count = result.count;
    data.rows = result.map(row => {
        return row.toJSON();
    });
    res.render('web/toothwash', data);
});

module.exports = router;