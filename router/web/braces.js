/**
 * Created by Administrator on 2018/3/29 0029.
 */
const router = require('express').Router();
const doctor = require('../../model/doctor');
const article = require('../../model/article');
const utils = require('../../utils/index');

router.get('/braces',async(req,res) =>{

    let doctorResult = null;
    let articleResult = null;
    try {
        [doctorResult, articleResult] = await Promise.all([
            doctor.findAndCountAll({
                limit: 20,
                offset: 0,
                order: [
                    ['sort', 'DESC'],
                    ['updatedAt', 'DESC']
                ]
            }),
            article.findAndCountAll({
                where:{
                    type:'牙齿矫正'
                },
                limit: 24,
                offset: 0,
                order: [
                    ['sort', 'DESC'],
                    ['updatedAt', 'DESC']
                ]
            }),
        ]);
    } catch (e) {
        doctorResult = {count: 0, rows: []};
        articleResult = {count: 0, rows: []};
        console.error(`/web/index 查询失败：${e.message}`);
    }

    let data = {};
    let htmlReg = /<((?:"[^"]*"|'[^']*'|[^'">])*)>/g;
    let srcReg = /\s+src="(\/public\/[^"]+)"/;

    //医生信息列表
    data.doctorCount = doctorResult.count;
    data.doctorRows = doctorResult.rows.map(row => {
        row = row.toJSON();
        row.content = row.content.replace(htmlReg, (_, val) => {
            if (!row.thumbnail && val.slice(0, 3) === 'img' && srcReg.test(val)) {
                row.thumbnail = RegExp.$1;
            }
            return '';
        });
        let d = row.updatedAt;
        row.updatedAt = `${d.getFullYear()}-${utils.formatNum(d.getMonth() + 1)}-${d.getDate()}`;
        return row;
    });

    //资讯信息列表
    data.articleCount = articleResult.count;
    // console.log(data.articleCount);
    data.articleRows = articleResult.rows.map(articleRow => {
        return articleRow.toJSON();
    });
    res.render('web/braces',data);
});

module.exports = router;