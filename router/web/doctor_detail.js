/**
 * Created by Administrator on 2018/3/29 0029.
 */
const router = require('express').Router();
const doctor = require('../../model/doctor');
const article = require('../../model/article');
const utils = require('../../utils/index');


router.get('/doctor_detail', async(req, res) => {
    let id = parseInt(req.query.id);
    if (!id){
        return res.redirect('/404');
    }
    let result = null;
    let articleList = null;
    try {
        [result,articleList] = await Promise.all([
            doctor.findById(id),
            article.findAll({
                limit: 10,
                offset: 0,
                order: [
                    ['sort', 'DESC'],
                    ['updatedAt', 'DESC']
                ]
            })
        ]);
    } catch (e) {
        result = {count:0,rows:[]};
        articleList = {count:0,rows:[]};
        console.error(`/web/doctor_detail 查询失败：${e.message}`);
    }
    let data = {};
    let htmlReg = /<((?:"[^"]*"|'[^']*'|[^'">])*)>/g;
    let srcReg = /\s+src="(\/public\/[^"]+)"/;

    data = result.toJSON();
    data.content = data.content.replace(htmlReg, (_, val) => {
        if (!data.thumbnail && val.slice(0, 3) === 'img' && srcReg.test(val)) {
            data.thumbnail = RegExp.$1;
        }
        return '';
    });

    let d = data.updatedAt;

    data.updatedAt = `${d.getFullYear()}-${utils.formatNum(d.getMonth() + 1)}-${d.getDate()}`;

    data.articleList = articleList.map(p =>{
        return p.toJSON();
    });

    res.render('web/doctor_detail', data);
});

module.exports = router;