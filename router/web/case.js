/**
 * Created by Administrator on 2018/3/29 0029.
 */

const router = require('express').Router();
const article = require('../../model/article');
const caseModel = require('../../model/case_model');
const utils = require('../../utils/index');

router.get('/case', async(req, res) => {
    let query = req.query;
    let page = parseInt(query.page) || 1;
    let size = parseInt(query.size) || 10;
    let result = null;
    let articleList = null;

    try {
        [result, articleList] = await Promise.all([
            caseModel.findAndCountAll({
                limit: size,
                offset: size * (page - 1),
                order: [
                    ['sort', 'DESC'],
                    ['updatedAt', 'DESC']
                ]
            }),
            article.findAll({
                limit: 10,
                offset: 0,
                order: [
                    ['sort', 'DESC'],
                    ['updatedAt', 'DESC']
                ]
            })
        ]);
    } catch (e) {
        articleList = [];
        result = {count: 0, rows: []};
        console.error(`/admin/our-teams 查询失败：${e.message}`);
    }

    let data = {};
    let htmlReg = /<((?:"[^"]*"|'[^']*'|[^'">])*)>/g;
    let srcReg = /\s+src="(\/public\/[^"]+)"/;

    data.count = result.count;
    data.rows = result.rows.map(row => {
        row = row.toJSON();
        row.content = row.content.replace(htmlReg, (_, val) => {
            if (!row.thumbnail && val.slice(0, 3) === 'img' && srcReg.test(val)) {
                row.thumbnail = RegExp.$1;
            }
            return '';
        });
        let d = row.updatedAt;
        row.updatedAt = `${d.getFullYear()}-${utils.formatNum(d.getMonth() + 1)}-${d.getDate()}`;
        return row;
    });

    data.page = page;
    data.size = size;
    //资讯列表
    data.list = articleList.map(p =>{
        // console.log(p.toJSON());
        return p.toJSON();
    });
    res.render('web/case', data);
});

module.exports = router;