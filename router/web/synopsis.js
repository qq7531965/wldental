/**
 * Created by Administrator on 2018/3/29 0029.
 */
const router = require('express').Router();

router.get('/synopsis', async(req, res) => {
    let data = {};
    res.render('web/synopsis', data);
});

module.exports = router;