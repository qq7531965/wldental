/**
 * Created by Administrator on 2018/3/29 0029.
 */
const router = require('express').Router();
const doctor = require('../../model/doctor');
const article = require('../../model/article');
const utils = require('../../utils/index');

router.get('/doctor', async(req, res) => {
    let query = req.query;
    let page = parseInt(query.page) || 1;
    let size = parseInt(query.size) || 5;
    let doctorResult = null;
    let articleList = null;

    try {
        [doctorResult, articleList] = await Promise.all([
            doctor.findAndCountAll({
                limit: size,
                offset: size * (page - 1),
                order: [
                    ['sort', 'DESC'],
                    ['updatedAt', 'DESC']
                ]
            }),
            article.findAll({
                limit: 10,
                offset: 0,
                order: [
                    ['sort', 'DESC'],
                    ['updatedAt', 'DESC']
                ]
            })
        ]);
    } catch (e) {
        doctorResult = {count: 0, rows: []};
        articleList = {count: 0, rows: []};
        console.error(`/admin/doctor 查询失败：${e.message}`);
    }
    let data = {};
    let htmlReg = /<((?:"[^"]*"|'[^']*'|[^'">])*)>/g;
    let srcReg = /\s+src="(\/public\/[^"]+)"/;

    data.resultCount = doctorResult.count;
    data.doctorResultRows = doctorResult.rows.map(row => {
        row = row.toJSON();

        row.content = row.content.replace(htmlReg, (_, val) => {
            if (!row.thumbnail && val.slice(0, 3) === 'img' && srcReg.test(val)) {
                row.thumbnail = RegExp.$1;
            }

            return '';
        });

        let d = row.updatedAt;

        row.updatedAt = `${d.getFullYear()}-${utils.formatNum(d.getMonth() + 1)}-${d.getDate()}`;

        return row;
    });

    data.page = page;
    data.size = size;

    data.articleList = articleList.map(p =>{
       return p.toJSON();
    });

    res.render('web/doctor', data);
});

module.exports = router;