const router  = require('express').Router();

router.get('/sitemap',(req,res)=>{
    res.render('web/sitemap');
});

module.exports = router;