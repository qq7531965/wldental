/**
 * Created by Administrator on 2018/3/27 0027.
 */
const router = require('express').Router();
const doctor = require('../../model/doctor');
const article = require('../../model/article');
const caseModel = require('../../model/case_model');
const utils = require('../../utils/index');

router.get('/', async(req, res) => {
    //医生团队
    let doctorResult = null;
    //种植中心
    let implant_fit = null;
    let implant_article = null;

    //牙齿矫正中心
    let braces_fit = null;
    let braces_article = null;

    //口腔修复中心
    let beauty_fit = null;
    let beauty_article = null;

    //口腔内科
    let medicine_fit = null;
    let medicine_article = null;

    //口腔外科
    let surgical_fit = null;
    let surgical_article = null;

    //儿童口腔
    let pediatric_fit = null;
    let pediatric_article = null;

    //健康宣传 新闻中心
    let health = null;
    let news_center = null;


    try {
        [doctorResult, implant_fit,implant_article,braces_fit,braces_article,beauty_fit,
            beauty_article,medicine_fit,medicine_article,surgical_fit,surgical_article,pediatric_fit,pediatric_article,health,news_center] = await Promise.all([
            doctor.findAndCountAll({
                limit: 20,
                offset: 0,
                order: [
                    ['sort', 'DESC'],
                    ['updatedAt', 'DESC']
                ]
            }),
            caseModel.findAndCountAll({
                where:{
                    type:'牙齿种植'
                },
                limit: 10,
                offset: 0,
                order: [
                    ['sort', 'DESC'],
                    ['updatedAt', 'DESC']
                ]
            }),
            article.findAndCountAll({
                where:{
                    type:'种植牙'
                },
                limit: 8,
                offset: 0,
                order: [
                    ['sort', 'DESC'],
                    ['updatedAt', 'DESC']
                ]
            }),
            caseModel.findAndCountAll({
                where:{
                    type:'牙齿矫正'
                },
                limit: 10,
                offset: 0,
                order: [
                    ['sort', 'DESC'],
                    ['updatedAt', 'DESC']
                ]
            }),
            article.findAndCountAll({
                where:{
                    type:'牙齿矫正'
                },
                limit: 8,
                offset: 0,
                order: [
                    ['sort', 'DESC'],
                    ['updatedAt', 'DESC']
                ]
            }),
            caseModel.findAndCountAll({
                where:{
                    type:'口腔修复'
                },
                limit: 10,
                offset: 0,
                order: [
                    ['sort', 'DESC'],
                    ['updatedAt', 'DESC']
                ]
            }),
            article.findAndCountAll({
                where:{
                    type:'牙齿修复'
                },
                limit: 8,
                offset: 0,
                order: [
                    ['sort', 'DESC'],
                    ['updatedAt', 'DESC']
                ]
            }),
            caseModel.findAndCountAll({
                where:{
                    type:'口腔内科'
                },
                limit: 10,
                offset: 0,
                order: [
                    ['sort', 'DESC'],
                    ['updatedAt', 'DESC']
                ]
            }),
            article.findAndCountAll({
                where:{
                    type:'牙周牙髓'
                },
                limit: 8,
                offset: 0,
                order: [
                    ['sort', 'DESC'],
                    ['updatedAt', 'DESC']
                ]
            }),
            caseModel.findAndCountAll({
                where:{
                    type:'口腔修复'
                },
                limit: 10,
                offset: 0,
                order: [
                    ['sort', 'DESC'],
                    ['updatedAt', 'DESC']
                ]
            }),
            article.findAndCountAll({
                where:{
                    type:'口腔外科'
                },
                limit: 8,
                offset: 0,
                order: [
                    ['sort', 'DESC'],
                    ['updatedAt', 'DESC']
                ]
            }),
            caseModel.findAndCountAll({
                where:{
                    type:'儿童牙科'
                },
                limit: 10,
                offset: 0,
                order: [
                    ['sort', 'DESC'],
                    ['updatedAt', 'DESC']
                ]
            }),
            article.findAndCountAll({
                where:{
                    type:'儿童牙科'
                },
                limit: 8,
                offset: 0,
                order: [
                    ['sort', 'DESC'],
                    ['updatedAt', 'DESC']
                ]
            }),
            article.findAndCountAll({
                where:{
                    type:'健康宣传'
                },
                limit: 4,
                offset: 0,
                order: [
                    ['sort', 'DESC'],
                    ['updatedAt', 'DESC']
                ]
            }),
            article.findAndCountAll({
                where:{
                    type:'新闻中心'
                },
                limit: 3,
                offset: 0,
                order: [
                    ['sort', 'DESC'],
                    ['updatedAt', 'DESC']
                ]
            })

        ]);
    } catch (e) {
        doctorResult = {count: 0, rows: []};
        implant_fit = {count: 0, rows: []};
        console.error(`/web/index 查询失败：${e.message}`);
    }

    let data = {};
    let htmlReg = /<((?:"[^"]*"|'[^']*'|[^'">])*)>/g;
    let srcReg = /\s+src="(\/public\/[^"]+)"/;

    //医生信息列表
    data.doctorCount = doctorResult.count;
    // console.log(data.doctorCount);
    data.doctorRows = doctorResult.rows.map(row => {
        row = row.toJSON();
        row.content = row.content.replace(htmlReg, (_, val) => {
            if (!row.thumbnail && val.slice(0, 3) === 'img' && srcReg.test(val)) {
                row.thumbnail = RegExp.$1;
            }
            return '';
        });
        let d = row.updatedAt;
        row.updatedAt = `${d.getFullYear()}-${utils.formatNum(d.getMonth() + 1)}-${d.getDate()}`;
        return row;
    });

    //种植中心
    data.implant_fit = implant_fit.rows.map(titleRow => {
        return titleRow.toJSON();
    });

    data.implant_article = implant_article.rows.map(titleRow => {
        return titleRow.toJSON();
    });

    //牙齿矫正中心
    data.braces_fit = braces_fit.rows.map(titleRow => {
        return titleRow.toJSON();
    });

    data.braces_article = braces_article.rows.map(titleRow => {
        return titleRow.toJSON();
    });

    //牙齿修复中心
    data.beauty_fit = beauty_fit.rows.map(titleRow => {
        return titleRow.toJSON();
    });

    data.beauty_article = beauty_article.rows.map(titleRow => {
        return titleRow.toJSON();
    });

    //口腔内科
    data.medicine_fit = medicine_fit.rows.map(titleRow => {
        return titleRow.toJSON();
    });

    data.medicine_article = medicine_article.rows.map(titleRow => {
        return titleRow.toJSON();
    });

    //口腔外科
    data.surgical_fit = surgical_fit.rows.map(titleRow => {
        return titleRow.toJSON();
    });

    data.surgical_article = surgical_article.rows.map(titleRow => {
        return titleRow.toJSON();
    });

    //儿童牙科
    data.pediatric_fit = pediatric_fit.rows.map(titleRow => {
        return titleRow.toJSON();
    });

    data.pediatric_article = pediatric_article.rows.map(titleRow => {
        return titleRow.toJSON();
    });

    //健康宣传 新闻中心

    data.health = health.rows.map(healthRow => {
        return healthRow.toJSON();
    });

    data.news_center = news_center.rows.map(row => {
        row = row.toJSON();
        row.content = row.content.replace(htmlReg, (_, val) => {
            if (!row.thumbnail && val.slice(0, 3) === 'img' && srcReg.test(val)) {
                row.thumbnail = RegExp.$1;
            }
            return '';
        });
        let d = row.updatedAt;
        row.updatedAt = `${d.getFullYear()}-${utils.formatNum(d.getMonth() + 1)}-${d.getDate()}`;
        return row;
    });



    res.render('web/index', data);
});

module.exports = router;