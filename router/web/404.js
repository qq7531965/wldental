/**
 * Created by Administrator on 2018/3/26 0026.
 */
const router = require('express').Router();

router.get('/404', (req, res)=> {
    res.render('web/404');
});

module.exports = router;