/**
 * Created by Administrator on 2018/2/27 0027.
 */

const redis = require('./model/redis');
const start = require('./start');
const account = require('./model/account');
const article = require('./model/article');
const caseModel = require('./model/case_model');
const doctor = require('./model/doctor');

//连接数据库,建立model
Promise.all([account.sync(), article.sync(), caseModel.sync(), doctor.sync(), redis.sync()]).then(start).catch(e => {
    console.log(`启动失败 :${e.stack}`);
});