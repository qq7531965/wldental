/**
 * Created by Administrator on 2018/3/26 0026.
 */
var Sequelize = require('sequelize');
var sequelize = require('./database.js');

module.exports = sequelize.define('case_model', {
    type: Sequelize.STRING,
    title: Sequelize.STRING,
    keyword: Sequelize.STRING,
    content: Sequelize.TEXT,
    sort: Sequelize.INTEGER

},{
    freezeTableName:true
});
