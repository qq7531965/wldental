/**
 * Created by Administrator on 2018/3/26 0026.
 */
let Sequelize = require('sequelize');
let sequelize = require('./database.js');

module.exports = sequelize.define('article', {
    type: Sequelize.STRING,
    title: Sequelize.STRING,
    keyword: Sequelize.STRING,
    content: Sequelize.TEXT,
    sort: Sequelize.INTEGER,
    clickNum: Sequelize.INTEGER
}, {
    freezeTableName: true
});
