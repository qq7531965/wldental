/**
 * Created by Administrator on 2018/3/26 0026.
 */
let Sequelize = require('sequelize');
let sequelize = require('./database.js');

module.exports = sequelize.define('doctor',{
    name :Sequelize.STRING,
    type:Sequelize.STRING,
    content:Sequelize.TEXT,
    skilled:Sequelize.STRING,
    duty:Sequelize.STRING,
    brief:Sequelize.STRING,
    sort:Sequelize.INTEGER
},{
    freezeTableName:true
});