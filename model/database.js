/**
 * Created by Administrator on 2018/3/26 0026.
 */
const Sequelize = require('sequelize');

const config = require('../config/database.config');

module.exports = new Sequelize(`postgres://${config.username}:${config.password}@${config.host}:${config.port}/${config.database}`, {
    logging: false
});
