/**
 * Created by Administrator on 2018/3/26 0026.
 */
const redis = require('redis');
const config = require('../config/redis.config');

const client = redis.createClient(config);

const promise = new Promise((resolve, reject) => {
    client.on('ready', () => {
        resolve();
    });
    client.on('error', (e) => {
        reject(e);
        console.error(`redis连接错误!:${e.message}`);
    })
});

client.sync = function () {
    return promise;
};

module.exports = client;