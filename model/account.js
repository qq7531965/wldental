/**
 * Created by Administrator on 2018/3/26 0026.
 */
let Sequelize = require('sequelize');

let sequelize = require('./database.js');

module.exports = sequelize.define('account', {
	username: Sequelize.STRING,
	password: Sequelize.STRING
}, {
	freezeTableName: true
});

