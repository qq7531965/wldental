/**
 * wlkq
 */
let gulp = require('gulp');
let rev = require('gulp-rev');
let revReplace = require('gulp-rev-replace');
let clean = require('gulp-clean'); //清空文件夹
let useref = require('gulp-useref');
let filter = require('gulp-filter');
let uglify = require('gulp-uglify');
let csso = require('gulp-clean-css');
let imagemin = require('gulp-imagemin');

//清空文件夹,避免资源冗余
gulp.task('clean', function () {
    return gulp.src('dist', {read: false})
        .pipe(clean());
});

//压缩JS文件
gulp.task('script',function () {
    //找到文件
    gulp.src('static/js/*.js')
        .pipe(uglify({mangle:false}))
        .pipe(gulp.dest('dist/static/js'))
});

gulp.task('router_admin',function () {
    //找到文件
    gulp.src('router/admin/*j.s')
        .pipe(uglify({mangle:false}))
        .pipe(gulp.dest('dist/router/admin'))
});

gulp.task('router_api',function () {
    //找到文件
    gulp.src('router/api/*j.s')
        .pipe(uglify({mangle:false}))
        .pipe(gulp.dest('dist/router/api'))
});

gulp.task('router_web',function () {
    //找到文件
    gulp.src('router/web/*j.s')
        .pipe(uglify({mangle:false}))
        .pipe(gulp.dest('dist/router/web'))
});

//压缩Css
gulp.task('css',function () {
    //找到文件
    gulp.src('static/css/*.css')
        .pipe(csso())
        .pipe(gulp.dest('dist/static/css'))
});


//图片压缩
gulp.task('image',function () {
    //找到文件
    gulp.src('static/img/*.*')
        .pipe(imagemin({progressive:true}))
        .pipe(gulp.dest('dist/static/img'))
});

//图片压缩
gulp.task('img-public',function () {
    //找到文件
    gulp.src('public/*.*')
        .pipe(imagemin({progressive:true}))
        .pipe(gulp.dest('dist/public/img2'))
});

//watch
gulp.task('auto',function () {
    gulp.watch('static/js/*.js',['script']);
    gulp.watch('static/css/*.css',['css']);
    gulp.watch('static/img/*.*',['image']);
    gulp.watch('router/admin/*.js',['script']);
    gulp.watch('router/api/*.js',['script']);
    gulp.watch('router/web/*.js',['script']);
});

gulp.task('default',['script','auto','css','image','router_admin','router_api','router_web']);




