/**
 * Created by Administrator on 2018/1/22 0022.
 */
module.exports = {
    formatNum (num){
        let p_num = parseInt(num);
        return (p_num >= 10 ? '' : '0') + p_num;
    },
    pagination(now, count, size, href){
        let htmls = [];
        let total = Math.ceil(count / size);

        //上一页
        if (now == 1) {
            htmls.push('<li><a href="javascript:;">&laquo;</a></li>');
        } else {
            htmls.push(`<li><a href="${href}?page=${now - 1}&size=${size}">&laquo;</a></li>`);
        }

        let start = now <= 5 ? 1 : (now - 3);

        //第一页
        if (start > 1) {
            htmls.push(`<li><a href="${href}?page=1&size=${size}">1</a></li><li><a href="javascript:;">...</a></li>`);
        }

        let end;

        if (total <= 5) {
            end = total;
        } else {
            end = Math.min(now + 2, total);
        }

        for (let i = start; i <= end; i++) {
            if (i == now) {
                htmls.push(`<li class="active"><a href="javascript::">${i}</a></li>`);
            }else{
                htmls.push(`<li><a href="${href}?page=${i}&size=${size}">${i}</a></li>`);
            }
        }

        if (end +1 <total){
            htmls.push('<li><a href="javascript:;">...</a></li>');
        }

        if (end != total){
            htmls.push(`<li><a href="${href}?page=${total}&size=${size}">${total}</a></li>`);
        }

        //下一页
        if (now == total){
            htmls.push('<li><a href="javascript:;">&raquo;</a></li>');
        }else{
            htmls.push(`<li><a href="${href}?page=${now + 1}&size=${size}">&raquo;</a></li>`);
        }

        return htmls.join('');
    }
};